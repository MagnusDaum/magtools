package com.magnusdaum.magtools;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

class AudioTools {
    private static final int RECORDER_SAMPLE_RATE_HZ = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private AudioRecord m_recorder = null;
    private boolean m_is_recording = false;
    private int m_buffer_size_bytes;
    private FFT m_fft;

    AudioTools() {
        final int min_buf_size_bytes = AudioRecord.getMinBufferSize(RECORDER_SAMPLE_RATE_HZ,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
        m_buffer_size_bytes = min_buf_size_bytes;

        Log.d("audiotools", "start_recording: buffer size bytes " + m_buffer_size_bytes);

        m_fft = new FFT(m_buffer_size_bytes / 2);
    }

    void start_recording() {
        m_recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLE_RATE_HZ,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, m_buffer_size_bytes);

        if (m_recorder.getState() == AudioRecord.STATE_INITIALIZED) {
            m_recorder.startRecording();
            m_is_recording = true;
        } else {
            Log.e("audiotools", "could not initialize audiorecord");
        }
    }

    void stop_recording() {
        if (null != m_recorder) {
            m_is_recording = false;
            m_recorder.stop();
            m_recorder.release();
            m_recorder = null;
        }
    }

    boolean is_recording() {
        return m_is_recording;
    }

    short[] get_audio_data() {
        short[] audio_data = new short[m_buffer_size_bytes / 2];
        if (m_is_recording) {
            m_recorder.read(audio_data, 0, m_buffer_size_bytes / 2);
        }
        return audio_data;
    }

    static double compute_decibels(double f_magnitude) {
        if (f_magnitude == 0.0) {
            return Double.NEGATIVE_INFINITY;
        } else {
            // divide by max 16 bit value to get ratio
            final double MAX_VALUE = 1 << 15;
            return 20 * Math.log10(Math.abs(f_magnitude) / MAX_VALUE);
        }
    }

    int compute_frequency_from_index(int f_index) {
        return f_index * RECORDER_SAMPLE_RATE_HZ / m_fft.get_signal_length();
    }

    int get_dominant_frequency_index(short[] f_audio_data) {
        double[] freq_amplitudes = m_fft.compute_fft(f_audio_data);
        return FFT.find_max_frequency_index(freq_amplitudes);
    }

    static class FFT {
        static class Complex {
            // helper class as Java does not support complex numbers
            double re;
            double im;

            Complex(double f_re, double f_im) {
                re = f_re;
                im = f_im;
            }

            static Complex exp(double f_iy) {
                // using euler's formula: exp(i*y) = cos(y) + i*sin(y)
                return new Complex(Math.cos(f_iy), Math.sin(f_iy));
            }

            static Complex mult(Complex f_lhs, Complex f_rhs) {
                // complex multiplication:
                // re = re1 * re2 - im1 * im2
                // im = re1 * im2 + im1 * re2
                double re = f_lhs.re * f_rhs.re - f_lhs.im * f_rhs.im;
                double im = f_lhs.re * f_rhs.im + f_lhs.im * f_rhs.re;
                return new Complex(re, im);
            }

            double abs() {
                return Math.sqrt(re * re + im * im);
            }
        }

        private int m_signal_length;
        private Complex[] m_lut;

        FFT(int f_signal_length) {
            // signal must be multiple of 2
            m_signal_length = 1 << (int) (Math.log(f_signal_length) / Math.log(2.0));

            // fill LUT once
            m_lut = compute_lut(m_signal_length);
        }

        int get_signal_length() {
            return m_signal_length;
        }

        static int find_max_frequency_index(double[] f_data) {
            int max_idx = 0;
            double max_val = f_data[0];
            for (int ii = 0; ii <= f_data.length / 2; ++ii) {
                if (f_data[ii] > max_val) {
                    max_val = f_data[ii];
                    max_idx = ii;
                }
            }
            return max_idx;
        }

        /// precompute lut for FFT. N is signal length (multiple of 2)
        private static Complex[] compute_lut(final int N) {
            final int lut_size = N * N / 2; // N/2 * N/2 * 2
            Complex[] lut = new Complex[lut_size];

            // Even: exp(-2*pi*k*n*i/N)
            // Odd:  exp(-2*pi*k*n*i/N) * exp(-2*pi*n*i/N)
            final double neg2pi_div_N = -2.0 * Math.PI / N;
            int lut_idx = 0;
            for (int kk = 0; kk < N; kk += 2) {
                final double neg2pi_k_div_N = neg2pi_div_N * kk;
                for (int nn = 0; nn < N / 2; ++nn) {
                    lut[lut_idx] = Complex.exp(neg2pi_k_div_N * nn);
                    final Complex cmpl = Complex.exp(neg2pi_div_N * nn);
                    lut[lut_idx + 1] = Complex.mult(cmpl, lut[lut_idx]);
                    lut_idx += 2;
                }
            }
            return lut;
        }

        double[] compute_fft(short[] f_input) {
            double[] output = new double[m_signal_length];
            if (m_signal_length <= f_input.length) {
                int n_div_2 = m_signal_length / 2;
                // precompute input terms
                double[] helper_even = new double[n_div_2];
                double[] helper_odd = new double[n_div_2];
                for (int nn = 0; nn < n_div_2; ++nn) {
                    helper_even[nn] = (double) (f_input[nn] + f_input[nn + n_div_2]);
                    helper_odd[nn] = (double) (f_input[nn] - f_input[nn + n_div_2]);
                }

                int lut_idx = 0;
                for (int kk = 0; kk < m_signal_length; kk += 2) {
                    Complex cmpl_even = new Complex(0.0, 0.0);
                    Complex cmpl_odd = new Complex(0.0, 0.0);
                    for (int nn = 0; nn < n_div_2; ++nn) {
                        cmpl_even.re += helper_even[nn] * m_lut[lut_idx].re;
                        cmpl_even.im += helper_even[nn] * m_lut[lut_idx].im;
                        cmpl_odd.re += helper_odd[nn] * m_lut[lut_idx + 1].re;
                        cmpl_odd.im += helper_odd[nn] * m_lut[lut_idx + 1].im;
                        lut_idx += 2;
                    }
                    output[kk] = cmpl_even.abs();
                    output[kk + 1] = cmpl_odd.abs();
                }
            }
            return output;
        }
    }

}
