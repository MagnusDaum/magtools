package com.magnusdaum.magtools;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HexToolsFragment extends Fragment {

    private final int BITS = 32;
    private long m_number = 0;
    private TextView[] m_bit_views = new TextView[BITS];
    private EditText m_et_dec = null;
    private EditText m_et_hex = null;
    private boolean m_text_watcher_blocked = false; // disable recursion in edittext change


    public HexToolsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hex_tools, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        m_et_hex = view.findViewById(R.id.et_hex);
        m_et_dec = view.findViewById(R.id.et_dec);

        m_et_hex.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!m_text_watcher_blocked) {
                    try {
                        set_number(Long.parseLong(m_et_hex.getText().toString(), 16));
                    } catch(NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        m_et_dec.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!m_text_watcher_blocked) {
                    try {
                        set_number(Long.parseLong(m_et_dec.getText().toString()));
                    } catch(NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        final Button btn_clear_all = view.findViewById(R.id.btn_clear_all);

        btn_clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset_fragment_focus();
                set_number(0);
            }
        });

        ViewGroup ll_binary = view.findViewById(R.id.ll_binary);
        for(int bit_idx = BITS - 1; bit_idx >= 0; --bit_idx) {
            TextView bit_view = new TextView(getContext());
            bit_view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            bit_view.setClickable(true);
            bit_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reset_fragment_focus();
                    int bit_idx = (int) v.getTag();
                    long new_number = get_number() ^ (1L << bit_idx); // toggle bit
                    set_number(new_number);
                }
            });
            bit_view.setTag(bit_idx);
            m_bit_views[bit_idx] = bit_view;
            ll_binary.addView(bit_view);
        }

        set_number(0);
    }

    private void reset_fragment_focus()
    {
        if (getActivity() != null) {
            View cur_view = getActivity().getCurrentFocus();
            if (cur_view != null) {
                cur_view.clearFocus();
            }
        }
    }

    private long get_number()
    {
        return m_number;
    }

    // todo make m_number own class and update views via listener
    private void set_number(long f_number)
    {
        m_number = f_number;

        // update views
        m_text_watcher_blocked = true;
        if (getActivity().getCurrentFocus() != m_et_dec) {
            m_et_dec.setText(m_number > 0 ? String.valueOf(m_number) : "");
        }
        if (getActivity().getCurrentFocus() != m_et_hex) {
            m_et_hex.setText(m_number > 0 ? Long.toHexString(m_number) : "");
        }
        for(int idx = 0; idx < BITS; ++idx)
        {
            final boolean is_bit_set = (m_number & (1 << idx)) != 0;
            m_bit_views[idx].setText(is_bit_set ? "1" : "0");
        }
        m_text_watcher_blocked = false;
    }

}
