package com.magnusdaum.magtools;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

public class TorchTools
{
    private boolean m_flash_on = false;
    private CameraManager m_camManager = null;
    private String m_cameraId = null;

    void setup_camera(CameraManager f_cameraManager)
    {
        try {
            m_camManager = f_cameraManager;
            // find cam with torch
            String[] cameras = m_camManager.getCameraIdList();
            for (String camId : cameras)
            {
                CameraCharacteristics cc = m_camManager.getCameraCharacteristics(camId);
                boolean flashAvailable = cc.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                if (flashAvailable) {
                    m_cameraId = camId;
                    break;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }


        CameraManager.TorchCallback torchCallback = new CameraManager.TorchCallback() {
            @Override
            public void onTorchModeUnavailable(@NonNull String cameraId) {
                super.onTorchModeUnavailable(cameraId);
            }

            @Override
            public void onTorchModeChanged(@NonNull String cameraId, boolean enabled) {
                super.onTorchModeChanged(cameraId, enabled);
                m_flash_on = enabled;
            }
        };
        m_camManager.registerTorchCallback(torchCallback, null);
    }

    void toggle(boolean f_on)
    {
        try {
            m_camManager.setTorchMode(m_cameraId, f_on);
            m_flash_on = f_on;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    void toggle_on()
    {
        toggle(true);
    }

    void toggle_off()
    {
        toggle(false);
    }

    void toggle()
    {
        toggle(!m_flash_on);
    }

    void activate_pwm(int f_duration_s, double f_frequency_hz, int f_duty_cycle)
    {
        final long timestamp_end = System.currentTimeMillis() + Math.max(f_duration_s, 0)*1000;

        final double period_s = f_frequency_hz > 0 ?  1.0 / f_frequency_hz : f_duration_s;
        final double dc = Math.max(0.0, Math.min(f_duty_cycle / 100.0, 1.0));
        final double t_on_s = period_s * dc;
        final double t_off_s = period_s - t_on_s;

        final double toggle_threshold_s = 0.001; // below that and it is not really reliable
        while(System.currentTimeMillis() < timestamp_end)
        {
            if (t_on_s > toggle_threshold_s) {
                toggle_on();
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (1000 * t_on_s));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
            if (t_off_s > toggle_threshold_s) {
                toggle_off();
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (1000 * t_off_s));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
        toggle_off();
    }
}
