package com.magnusdaum.magtools;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewPager = findViewById(R.id.vp_main);

        MainPagerAdapter adapter = new MainPagerAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tl_main);
        tabLayout.setupWithViewPager(viewPager);
    }
}

class MainPagerAdapter extends FragmentPagerAdapter {
    MainPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
    }

    private static final CharSequence[] m_page_titles = {"Torch", "Hex", "Audio"};

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0: return new TorchToolsFragment();
            case 1: return new HexToolsFragment();
            case 2: return new AudioToolsFragment();
            default: return new Fragment();
        }
    }

    @Override
    public int getCount() {
        return m_page_titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (0 <= position && position < m_page_titles.length)
        {
            return m_page_titles[position];
        }
        return "";
    }
}