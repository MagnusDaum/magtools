package com.magnusdaum.magtools;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class AudioToolsFragment extends Fragment {

    private AudioTools m_audio_tools = new AudioTools();
    private Handler m_handler = new Handler();

    public AudioToolsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_audio_tools, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, 1);
        }

        final Button btn = view.findViewById(R.id.btn_fft);
        final TextView tv_tmp = view.findViewById(R.id.tv_tmp);
        final TextView tv_tmp2 = view.findViewById(R.id.tv_tmp2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!m_audio_tools.is_recording()) {
                    m_audio_tools.start_recording();
                    Thread rec_thread = new Thread(new Runnable() {
                        public void run() {
                            while (m_audio_tools.is_recording()) {
                                short[] audio_data = m_audio_tools.get_audio_data();
                                short val = audio_data[0];
                                short val2 = audio_data[0];
                                for (short ad : audio_data) {
                                    if (ad > val) val = ad;
                                    if (ad < val2) val2 = ad;
                                }
                                System.out.println("maximum audio record value " + val
                                        + " (= " + AudioTools.compute_decibels(val) + " db)");
                                System.out.println("minimum audio record value " + val2
                                        + " (= " + AudioTools.compute_decibels(val2) + " db)");
                                final double max_db =
                                        AudioTools.compute_decibels(Math.max(val, Math.abs(val2)));

                                final int freq_idx =
                                        m_audio_tools.get_dominant_frequency_index(audio_data);
                                final int freq =
                                        m_audio_tools.compute_frequency_from_index(freq_idx);
                                System.out.println("dom freq " + freq + " (= " + AudioTools.compute_decibels(audio_data[freq_idx]) + " db)");

                                m_handler.post(new Runnable() {
                                    public void run() {
                                        tv_tmp.setText(String.valueOf(Math.round(max_db*1000.0)/1000.0));
                                        tv_tmp2.setText(String.valueOf(freq));
                                    }
                                });
                            }
                        }
                    });
                    rec_thread.start();
                } else {
                    m_audio_tools.stop_recording();
                }
            }
        });
    }

}
